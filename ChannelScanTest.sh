#!/bin/bash
Gre='\e[0;32m'; On_Red='\e[41m'
RCol='\e[0m';
cd sdcard/Automation
settings put secure sleep_timeout -1

Snum=$(getprop ro.serialno)
MacA=$(getprop ro.boot.mac)
Build=$(getprop ro.product.version.base)
Launcher=$(dumpsys package com.dotscreen.megacable.livetv | grep versionName | cut -d "=" -f2)
RefCH=368
#Prints device under test information.

echo "Serial Number","$Snum" >> ScanResults
echo "MAC Address","$MacA" >> ScanResults
echo "Build","${Build:0:6}" >> ScanResults
echo "Launcher","${Launcher:0:6}" >> ScanResults
echo "Tiemstamp,Test#,Channels Found,Issue" >> ScanResults

run_profiles() {
  Profiles=$(dumpsys window windows | grep -E mCurrentFocus)
  if [[ $Profiles == *"ProfileManagementActivity"* ]]; then
    sleep 1
    input keyevent KEYCODE_BACK      #Closes PIN menu if opened
    input keyevent KEYCODE_DPAD_DOWN #LOCATES SELECTION TO MAIN PROFILE
    input keyevent KEYCODE_DPAD_LEFT
    input keyevent KEYCODE_DPAD_LEFT
    input keyevent KEYCODE_DPAD_CENTER
    sleep 1
    input text "0000"
  fi
  return
}

run_profiles
    
echo "------------------------------"
echo "Running Channel Scan Test v1.0"
echo "------------------------------"

while : 
  do
    run_profiles
    input keyevent KEYCODE_HOME
    for i in $(seq 7); do
    input keyevent KEYCODE_DPAD_DOWN
    done
    for i in $(seq 4); do
    input keyevent KEYCODE_DPAD_LEFT
    done
    input keyevent KEYCODE_DPAD_CENTER
    for i in $(seq 4); do
    input keyevent KEYCODE_DPAD_RIGHT
    done
    input keyevent KEYCODE_DPAD_DOWN
    input keyevent KEYCODE_DPAD_CENTER
    timestamp=$(date "+%Y-%m-%d %H:%M:%S")
    sleep 15
    run_profiles

    Scan=$(logcat -d | grep SCAN_STATUS_TIF_UPDATE_DONE)
    if [[ $Scan == *"SCAN_STATUS_OK"* ]]; then
      if [[ ${Scan:(-3)} = $RefCH ]]; then
        echo -n -e $Gre "\e[0K\rChannels found:$RCol ${Scan:(-3)}"
        echo "$timestamp,$n,${Scan:(-3)}" >> ScanResults
      fi
      if [[ ${Scan:(-3)} < $RefCH ]]; then
        echo -n -e $On_Red "\e[0K\rChannels found:$RCol ${Scan:(-3)} Channel loss Detected!"
        echo "$timestamp,$n,${Scan:(-3)},Channel loss" >> ScanResults
      fi
      if [[ ${Scan:(-3)} > $RefCH ]]; then
        echo -n -e $On_Red "\e[0K\rChannels found:$RCol ${Scan:(-3)} Channel increase Detected!"
        echo "$timestamp,$n,${Scan:(-3)},Channel increase" >> ScanResults
      fi
      RefCH=${Scan:(-3)}
    fi
    Scan1=$(logcat -d | grep SCAN_STATUS_ERROR)
    if [[ $Scan1 == *"No channel found"* ]]; then
      echo -n -e $On_Red "\e[0K\rChannel Scan Fail"$RCol
      echo "$timestamp,$n,fail" >> ScanResults
    fi
    Scan2=$(logcat -d | grep SCAN_STATUS_ERROR)
    if [[ $Scan2 == *"null"* ]]; then
      echo -n -e $On_Red "\e[0K\rChannel Scan Fail"$RCol
      echo "$timestamp,$n,fail" >> ScanResults
    fi
    logcat -c  
    n=$(($n+1))
    sleep 105
  done
exit