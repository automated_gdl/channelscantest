@ECHO OFF
::
PATH=%PATH%
echo Running automated channel scan test.
echo "Scan results" > ScanResults
set start_date=%DATE%
goto loop
::Test git
:reset
taskkill /fi "WindowTitle eq Channel scan test*"
FOR /F %%x IN (Devices.txt) DO (
    mkdir "C:\Users\QA-Automation\Google Drive\QA-Automation-Android-GDL\ATR\ChannelScanTest\%DATE%\STB-%%x"
    adb -s %%x shell mkdir sdcard/Automation/
    adb -s %%x push ChannelScanTest.sh ScanResults /sdcard/Automation/
)
set start_date=%DATE%

:loop
FOR /F %%x IN (Devices.txt) DO (
    tasklist /FI "WINDOWTITLE eq Channel scan test %%x" | findstr /B "INFO:" > nul && start "Channel scan test %%x" /HIGH cmd /c "adb -s %%x shell sh /sdcard/Automation/ChannelScanTest.sh"  
    adb -s %%x pull /sdcard/Automation/ScanResults "C:\Users\QA-Automation\Google Drive\QA-Automation-Android-GDL\ATR\ChannelScanTest\%DATE%\STB-%%x\ScanResults.csv"
    timeout /t 1 /nobreak > nul
)  

if %start_date% NEQ %DATE% goto reset 
timeout /t 60 /nobreak > nul
goto loop

echo Press any key to exit...
pause >nul
exit